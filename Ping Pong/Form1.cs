﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ping_Pong
{
    public partial class Form1 : Form
    {
        private int _directionX = 5;
        private int _directionY = 2;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            //(throw new System.NotImplementedException();
        }


        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrSpiel.Enabled = true;
            tmrSpiel.Start();
        }

        private void tmrSpiel_Tick(object sender, EventArgs e)
        {

            
            
            Point actual = picBall.Location;
            //Point ist der Datentyp, actual ist eine Variable, picBall der BAll und .Location die Postition des Balles

            picBall.Location = new Point(picBall.Location.X + _directionX, picBall.Location.Y + _directionY);

            if (picBall.Location.X >= panel1.Width - picBall.Width)
            {
                _directionX = -_directionX;
            }

            if (picBall.Location.X <= 0)
            {
                _directionX = -_directionX;
                //ps. das "-" ist richttig, den - mal- ergibt +.
            }

            if (picBall.Location.Y >= panel1.Height - picBall.Height)
            {
                _directionY = -_directionY;
            }

            if (picBall.Location.Y <= 0)
            {
                _directionY = -_directionY;
            }
        }

        private void picBall_Click(object sender, EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            //throw new System.NotImplementedException();
        }
        
    }
}