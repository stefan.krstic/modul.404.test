﻿namespace Ping_Pong
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Schläger = new System.Windows.Forms.PictureBox();
            this.picBall = new System.Windows.Forms.PictureBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.tmrSpiel = new System.Windows.Forms.Timer(this.components);
            this.SchlägerSteuerrad = new System.Windows.Forms.VScrollBar();
            this.PunkteEffektive = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.Schläger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.picBall)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SeaGreen;
            this.panel1.Controls.Add(this.Schläger);
            this.panel1.Controls.Add(this.picBall);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(625, 425);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Schläger
            // 
            this.Schläger.BackColor = System.Drawing.Color.Black;
            this.Schläger.Location = new System.Drawing.Point(618, 134);
            this.Schläger.Name = "Schläger";
            this.Schläger.Size = new System.Drawing.Size(3, 40);
            this.Schläger.TabIndex = 1;
            this.Schläger.TabStop = false;
            // 
            // picBall
            // 
            this.picBall.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.picBall.Location = new System.Drawing.Point(400, 0);
            this.picBall.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.picBall.Name = "picBall";
            this.picBall.Size = new System.Drawing.Size(22, 18);
            this.picBall.TabIndex = 0;
            this.picBall.TabStop = false;
            this.picBall.Click += new System.EventHandler(this.picBall_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(20, 456);
            this.btnStart.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(117, 36);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Spiel starten";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tmrSpiel
            // 
            this.tmrSpiel.Interval = 10;
            this.tmrSpiel.Tick += new System.EventHandler(this.tmrSpiel_Tick);
            // 
            // SchlägerSteuerrad
            // 
            this.SchlägerSteuerrad.Location = new System.Drawing.Point(626, 0);
            this.SchlägerSteuerrad.Name = "SchlägerSteuerrad";
            this.SchlägerSteuerrad.Size = new System.Drawing.Size(25, 425);
            this.SchlägerSteuerrad.TabIndex = 2;
            this.SchlägerSteuerrad.Value = 50;
            this.SchlägerSteuerrad.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll);
            // 
            // PunkteEffektive
            // 
            this.PunkteEffektive.Location = new System.Drawing.Point(290, 464);
            this.PunkteEffektive.Name = "PunkteEffektive";
            this.PunkteEffektive.Size = new System.Drawing.Size(100, 23);
            this.PunkteEffektive.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 504);
            this.Controls.Add(this.PunkteEffektive);
            this.Controls.Add(this.SchlägerSteuerrad);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "Form1";
            this.Text = "Ping Pong Spiel";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.Schläger)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.picBall)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Timer tmrSpiel;
        private System.Windows.Forms.PictureBox picBall;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.VScrollBar SchlägerSteuerrad;
        private System.Windows.Forms.PictureBox Schläger;
        private System.Windows.Forms.TextBox PunkteEffektive;
    }
}