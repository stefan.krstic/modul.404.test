﻿namespace pingpong.v2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.schlaeger = new System.Windows.Forms.PictureBox();
            this.picBall = new System.Windows.Forms.PictureBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.tmrSpiel = new System.Windows.Forms.Timer(this.components);
            this.schlaegerSteuerrad = new System.Windows.Forms.VScrollBar();
            this.PunkteEffektive = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.schlaeger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.picBall)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SeaGreen;
            this.panel1.Controls.Add(this.schlaeger);
            this.panel1.Controls.Add(this.picBall);
            this.panel1.Location = new System.Drawing.Point(2, 112);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(715, 600);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // schlaeger
            // 
            this.schlaeger.BackColor = System.Drawing.Color.Black;
            this.schlaeger.Location = new System.Drawing.Point(706, 179);
            this.schlaeger.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.schlaeger.Name = "schlaeger";
            this.schlaeger.Size = new System.Drawing.Size(3, 54);
            this.schlaeger.TabIndex = 1;
            this.schlaeger.TabStop = false;
            // 
            // picBall
            // 
            this.picBall.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.picBall.Location = new System.Drawing.Point(457, 0);
            this.picBall.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.picBall.Name = "picBall";
            this.picBall.Size = new System.Drawing.Size(25, 24);
            this.picBall.TabIndex = 0;
            this.picBall.TabStop = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(13, 795);
            this.btnStart.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(134, 48);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Spiel starten";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tmrSpiel
            // 
            this.tmrSpiel.Interval = 10;
            this.tmrSpiel.Tick += new System.EventHandler(this.tmrSpiel_Tick);
            // 
            // schlaegerSteuerrad
            // 
            this.schlaegerSteuerrad.LargeChange = 1;
            this.schlaegerSteuerrad.Location = new System.Drawing.Point(719, 91);
            this.schlaegerSteuerrad.Name = "schlaegerSteuerrad";
            this.schlaegerSteuerrad.Size = new System.Drawing.Size(25, 644);
            this.schlaegerSteuerrad.TabIndex = 2;
            this.schlaegerSteuerrad.Scroll +=
                new System.Windows.Forms.ScrollEventHandler(this.SchlägerSteuerrad_Scroll);
            // 
            // PunkteEffektive
            // 
            this.PunkteEffektive.Location = new System.Drawing.Point(313, 805);
            this.PunkteEffektive.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PunkteEffektive.Name = "PunkteEffektive";
            this.PunkteEffektive.Size = new System.Drawing.Size(114, 27);
            this.PunkteEffektive.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 859);
            this.Controls.Add(this.PunkteEffektive);
            this.Controls.Add(this.schlaegerSteuerrad);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.Name = "Form1";
            this.Text = "Ping Pong Spiel";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.schlaeger)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.picBall)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Timer tmrSpiel;
        private System.Windows.Forms.PictureBox picBall;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox PunkteEffektive;
        private System.Windows.Forms.PictureBox schlaeger;
        private System.Windows.Forms.VScrollBar schlaegerSteuerrad;
    }
}