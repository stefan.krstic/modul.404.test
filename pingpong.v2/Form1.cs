﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace pingpong.v2
{
    public partial class Form1 : Form
    {
        private int _directionX = 5;
        private int _directionY = 2;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            //(throw new System.NotImplementedException();
        }


        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrSpiel.Enabled = true;
            tmrSpiel.Start();
        }

        private void tmrSpiel_Tick(object sender, EventArgs e)
        {

            
            
            
            
            //Point ist der Datentyp, actual ist eine Variable, picBall der BAll und .Location die Postition des Balles

            picBall.Location = new Point(picBall.Location.X + _directionX, picBall.Location.Y + _directionY);

            if (picBall.Location.X >= panel1.Width - picBall.Width)
            {
                _directionX = -_directionX;
            }

            if (picBall.Location.X <= 0)
            {
                _directionX = -_directionX;
                //ps. das "-" ist richttig, den - mal- ergibt +.
            }

            if (picBall.Location.Y >= panel1.Height - picBall.Height)
            {
                _directionY = -_directionY;
            }

            if (picBall.Location.Y <= 0)
            {
                _directionY = -_directionY;
            }
        }


        private void SchlägerSteuerrad_Scroll(object sender, ScrollEventArgs e)
        {
            var hoehe =  panel1.Height / 100 * schlaegerSteuerrad.Value;

            
            // panel1.height ist falsch weil es wert 0 ist wir brauchen den max und nicht den anfang. druch 100 mal schlaegerSteuerrad.Value soll den Prozentwert wiederspiegeln auf das feld. 
            var newPosition = new Point(schlaeger.Location.X, hoehe);
            // x bleibt immer gleich und y immer anhand vor position des scrollers mit der variable hoehe. Point ist eine KLasse für die position für 2d sachen.
            schlaeger.Location = newPosition;
            // 

           
            // new Point(SchlägerSteuerrad., SchlägerSteuerrad.Location.Y);
        }

        private void schlaeger_Click(object sender, EventArgs e)
        {
            throw new System.NotImplementedException();
        }
    }
}